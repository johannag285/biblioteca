$(document).ready(function(){



    $('#example').DataTable({

    "language": {
      "decimal":        "",
      "emptyTable":     "No hay datos disponibles en la tabla",
      "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
      "infoEmpty":      "Mostrando 0 to 0 of 0 entradas",
      "infoFiltered":   "(filtrando desde _MAX_ total entradas)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ entradas",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros coincidentes",
  "paginate": {
      "first":      "Primero",
      "last":       "Último",
      "next":       "Siguiente",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": activate to sort column ascending",
      "sortDescending": ": activate to sort column descending"
  }
    },
    "columnDefs": [
       { "visible": false, "targets": 6 },
       { "visible": false, "targets": 7 },
       { "visible": false, "targets": 8 }
      
    ]
  });



  var table = $('#example').DataTable();
  $('#example').on('click', 'tbody .devolver', function(e){
      var data_devolver = new Object();
      data_devolver.idUserLib = table.row($(this).closest('tr')).data()[6];
      data_devolver.idLib = table.row($(this).closest('tr')).data()[7];
      data_devolver.cant= table.row($(this).closest('tr')).data()[8];
      data_devolver.devolver = 1;

      $.ajax({
        url: 'misLibros.php',
        method: 'POST',
        data: data_devolver,
        success:function(response){
            //alert(response);
            alert("Libro devuelto");
            location.href = "misLibros.php";
          },error: function(e){
              //console.log('Error! ');
              alert("No se pudo devolver el libro");
            }
            
      });
      console.log(data_devolver);
  });
 
//devolver el libro solo hasta que lo devuelva puede volver a prestar uno, no puede prestar mas de un libro
});