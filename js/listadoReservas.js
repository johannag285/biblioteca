$(document).ready(function(){



    $('#example').DataTable({

    "language": {
      "decimal":        "",
      "emptyTable":     "No hay datos disponibles en la tabla",
      "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
      "infoEmpty":      "Mostrando 0 to 0 of 0 entradas",
      "infoFiltered":   "(filtrando desde _MAX_ total entradas)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ entradas",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros coincidentes",
  "paginate": {
      "first":      "Primero",
      "last":       "Último",
      "next":       "Siguiente",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": activate to sort column ascending",
      "sortDescending": ": activate to sort column descending"
  }
    },
    "columnDefs": [
      { "visible": false, "targets": 0 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 1 },
      { "visible": false, "targets": 8 }
    ]
  });



  var table = $('#example').DataTable();
 $('#example').on('click', 'tbody .prestar', function(e){
     var data_reserva = new Object();
     data_reserva.id = table.row($(this).closest('tr')).data()[0];
     data_reserva.idLib = table.row($(this).closest('tr')).data()[1];
     data_reserva.prestar = 0;
     data_reserva.cant = table.row($(this).closest('tr')).data()[3];
     data_reserva.estado = table.row($(this).closest('tr')).data()[7];
    
     $.ajax({
        url: 'listadoReservas.php',  
        method : 'POST',
        data: data_reserva,
        success:function(response){
            //alert(response);
            alert("Libro prestado");
            location.href="listadoReservas.php";
          },error: function(e){
              //console.log('Error! ');
              alert("No se pudo prestar el libro");
        }
     });

 });

//los que solicite y ya me prestaron 
});