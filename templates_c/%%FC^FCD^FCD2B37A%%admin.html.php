<?php /* Smarty version 2.6.19, created on 2019-08-01 15:37:00
         compiled from admin.html */ ?>
<head>

    <link rel="stylesheet" type="text/css" href="../../lib/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    
    <link href="https://fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css" />
    <script type="text/javascript" src="../../lib/jquery/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="js/tabla_usuarios.js"></script>

    <meta charset="utf-8">
    <title>Administrador</title>
</head>

<body>
    <h1>Administrador</h1>
    <a href="logout.php">Cerrar sesión</a> <br>
    <a href="usuarios.php" id="listadoAdmin" >Ver listado de usuarios</a> <br>
    <a href="listadoReservas.php" id="listadoAdmin" >Ver listado de reservas</a>

    <!--
<div class="col-md-3">
    
        <div class="list-group"  >
                <a href="#" class="list-group-item active" id="menu">Menu Admin</a> 
                <a href="usuarios.php" id="listadoAdmin" class="list-group-item item">Ver listado de usuarios</a> 
                <a href="listadoReservas.php" id="listadoAdmin" class="list-group-item item">Ver listado de reservas</a>
        </div>
</div>-->
<br>
<br>
<br>

    <table id="example" class="col-md-8 table table-striped table-bordered " style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Género</th>
                <th>Cantidad Disponible</th>
                <th>Autor</th>
                <th>Editorial</th>
                <th>Descripción</th>
                
                <th>Acciones</th>
            </tr>

        <tbody>
        
            <?php $_from = $this->_tpl_vars['libros']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['libro']):
?>
              
            <tr>
                <td><?php echo $this->_tpl_vars['libro'][1]; ?>
</td>
                <td><?php echo $this->_tpl_vars['libro'][2]; ?>
</td>
                <td><?php echo $this->_tpl_vars['libro'][3]; ?>
</td>
                <td><?php echo $this->_tpl_vars['libro'][4]; ?>
</td>
                <td><?php echo $this->_tpl_vars['libro'][5]; ?>
</td>
                <td><?php echo $this->_tpl_vars['libro'][6]; ?>
</td>
                
                
                
                <td>
                    <a href="./editarLibro.php?id=<?php echo $this->_tpl_vars['libro'][0]; ?>
" class="btn btn-info" name="editar" id="editar">Editar</a>

                    
                </td>



            </tr>
            <?php endforeach; endif; unset($_from); ?>
          

            
        </tbody>


        </thead>
    </table>
    <a href="./agregarLibro.php" class="btn btn-info"  name="editar" id="editar">Agregar nuevo libro</a>
</body>

</html>