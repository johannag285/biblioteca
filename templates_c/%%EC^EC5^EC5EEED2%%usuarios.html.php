<?php /* Smarty version 2.6.19, created on 2019-08-01 16:06:10
         compiled from usuarios.html */ ?>
<html>

<head>

    <link rel="stylesheet" type="text/css" href="../../lib/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css" />
    <script type="text/javascript" src="../../lib/jquery/jquery-1.12.2.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="js/usuarios.js"></script>

    <meta charset="utf-8">
    <title>Tabla usuarios</title>
</head>

<body >
  
        <h1 href="#">Listado de usuarios</h1>
   

    <a href="logout.php">Cerrar sesión</a>
    <br> <br>
    <div class="container">
       
        
     
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Avatar</th>
                    <th>Correo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Fecha nac</th>
                    <th>Edad</th>
                    <th>Telefono</th>
                    <th>Dirección</th>
                    
                    <!--  <th id="acciones">Acciones</th> -->
                   
                </tr>
               
            <tbody>

                    

                   
                    <?php $_from = $this->_tpl_vars['usuarios']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['curr_id']):
?>
                    <tr>
                       <td  id="column1">  <img id="avatar" src="<?php echo $this->_tpl_vars['curr_id'][8]; ?>
" /> </td> 
                       <td  id="column2" ><?php echo $this->_tpl_vars['curr_id'][1]; ?>
 </td> 
                       <td  id="column3"><?php echo $this->_tpl_vars['curr_id'][3]; ?>
</td> 
                       <td  id="column4"><?php echo $this->_tpl_vars['curr_id'][4]; ?>
 </td> 
                       <td  id="column5"><?php echo $this->_tpl_vars['curr_id'][10]; ?>
 </td>
                       <td  id="column6"><?php echo $this->_tpl_vars['curr_id'][9]; ?>
 </td> 
                       <td  id="column7"><?php echo $this->_tpl_vars['curr_id'][5]; ?>
</td>
                       <td  id="column8"><?php echo $this->_tpl_vars['curr_id'][6]; ?>
</td>
                       <!-- 
                       <td>
                           <a href="./editar.php?id=<?php echo $this->_tpl_vars['curr_id'][0]; ?>
" class="btn btn-info" name="editar" id="editar">Editar</a>
                           <a href = "./usuarios.php?idu=<?php echo $this->_tpl_vars['curr_id'][0]; ?>
" class="btn btn-danger">Eliminar</a>
                           
                       </td>-->
                   </tr>
                   <?php endforeach; endif; unset($_from); ?>

                   
                  
                  
            </tbody>
           
           
            </thead>

           
        </table>

        
    </div>

</body>

<button type="submit" class="btn btn-info " id="admin" name="admin" value = 'admin'>Crear Admin</button>
<button type="submit" class="btn btn-warning usuario" id="user" name="user" value = 'user'>Crear usuario</button>



</html>