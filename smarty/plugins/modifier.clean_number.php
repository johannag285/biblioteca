<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty count_sentences modifier plugin
 *
 * Type:     modifier<br>
 * Name:     count_sentences
 * Purpose:  count the number of sentences in a text
 * @link http://smarty.php.net/manual/en/language.modifier.count.paragraphs.php
 *          count_sentences (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return integer
 */
function smarty_modifier_clean_number($string)
{
    // find periods with a word before but not after.
    $number=trim($string);
    $number=str_replace('$','',$number);
    $number=str_replace('.','',$number);    
    $number=str_replace(",",'',$number);
    //$number=ereg_replace("[^0-9]","",$number);
    return intval($number);
}

/* vim: set expandtab: */

?>
