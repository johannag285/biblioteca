<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty count_sentences modifier plugin
 *
 * Type:     modifier<br>
 * Name:     count_sentences
 * Purpose:  count the number of sentences in a text
 * @link http://smarty.php.net/manual/en/language.modifier.count.paragraphs.php
 *          count_sentences (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return integer
 */
function smarty_modifier_examina_valor($cadena="",$inicial=0)
{
    // find periods with a word before but not after.
    $cadena_retorno="";
    $encuentra_por=strpos($cadena,'%');
    if($encuentra_por===false)
    {
		$cadena_retorno=intval($cadena);
	}
	else
	{		
		$porcentaje=substr($cadena,0,$encuentra_por);
		$cadena_retorno=(intval($inicial)*intval($porcentaje) )/100;
	}
    return '$'.number_format($cadena_retorno);
}

/* vim: set expandtab: */

?>
