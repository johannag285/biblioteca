<?php
//ISSET omprueba si una variable está definida o no en el script de PHP que se está ejecutando.
//inicia sesion --- inicia una nueva sesion o la reanuda
session_start();

//si no hay algo almacenado se redirecciona al login
if(!isset($_SESSION["usuario"])){
    //redireccionamos a la pagina del login
    header("location: login.php");
}
?>