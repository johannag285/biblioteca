<?php
 include('database.php');
 include("smarty/Smarty.class.php");
 $smarty = new Smarty();



 try{
	 //Iniciar nueva o reanudar sesión existente
	session_start();
	//variables
	$email =  isset($_POST['email']);
	$pass =  isset($_POST['password']);
	$submit =  isset($_POST['enviar']);
	$login = "";
	$password = "";
	
//si existe cerrar_sesion
if(isset($_GET['cerrar_sesion'])){
  //Libera todas las variables de sesión
  session_unset();
  session_destroy();

}
 //exit;

//si existe un rol entonces se redirecciona
 if(isset($_SESSION['rol'])){

	switch($_SESSION['rol']){
		case 1:
		header('location: admin.php');
		break;
         
		case 2:
		header('location: user.php');
		break;

		default:
	}

}



 //valiidamos que exista un correo y un pass
 if($email != "" && $pass != ""){
	   //htmlentities=convierte cualquier simbolo en html. addcslashes = no tiene encuenta los caracteres
       $login = htmlentities(addslashes($_POST['email']));
	   $password= htmlentities(addslashes($_POST['password']));
	   

	   //en la variable registros asigno la consulta a la base de datos 
       $registros = "SELECT * FROM  REGISTRO_USUARIOS.USUARIO WHERE correo = :nom AND contraseña = :pass";
       //se crea una variable llamada resultado en donde se le asigna la conexion que llama la funcion prepare
	   $resultado=$conn->prepare($registros);
	   

	   //la variable resultado llama a la función binValue y establecemos la equivalencia entre los marcadores y las variables
 
       $resultado->bindValue(":nom", $login);
	   $resultado->bindValue(":pass",$password);
	   
	    //ejecutamos ---- resultado llama a la función execute
	   $resultado->execute();
	   //la consulta que trae los datos se transformen a un arreglo
	   $row = $resultado->fetch(PDO::FETCH_NUM);

// validamos si existe datos en el arreglo
	if($row == true){
	    //validamos el rol
		$rol = $row[7];
		$id = $row[0];
	   //asignamos el rol que tiene a la session
		$_SESSION["rol"] = $rol;
		$_SESSION["id"] = $id;
		
	
		switch(isset($_SESSION['rol'])){
		   case 1:
		   header('location: admin.php');
		   break;
			
		   case 2:
		   header('location: user.php');
		   break;
   
		   default:
	   }
	   
	   //header("location:usuarios.php");
   }else{
	   echo("el usuario es incorrecto");
   }

}

 
}catch(Exception $e){
	
	die("Error: "  . $e->getMessage());
}

$smarty->display("header.html");
$smarty->display("login.html");